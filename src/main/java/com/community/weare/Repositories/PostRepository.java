package com.community.weare.Repositories;

import com.community.weare.Models.Category;
import com.community.weare.Models.Post;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer>,
        JpaSpecificationExecutor<Post> {

    List<Post> findAllByUserUsername(Sort sort, String userName);

    Slice<Post> findAllByUserUsername(Pageable pageRequest, String userName);

    @Query(value = "SELECT p from Post as p where p.user.username = :userName and p.isPublic = true")
    Slice<Post> findAllByUserUsernamePublic(Pageable pageRequest, @Param("userName") String userName);

    @Query(value = "select p from Post as p where p.isPublic = true")
    Slice<Post> findAllPublicPosts(Pageable pageable);

    @Query(value = "select p from Post as p where p.user.expertiseProfile.category.name = :categoryName and p.isPublic = true")
    Slice<Post> findAllByCategoryNameAndPublic(Pageable pageable, @Param("categoryName")String categoryName);

//    @Query(value = "select p from Post as p " +
//            "where p.user in (select u.friendList from User as u where u.userId = :userId)")
    @Query(value = "select p from Post as p " +
            "where p.user in (select b from User as u join u.friendList as b where u.userId = :userId)")
    Slice<Post> findAllFriendsPosts(Pageable pageable, int userId);
}


